/**
 * util/index.js
 *
 * @description :: utils functions
 * @docs        :: TODO
 */
const attributeNamePrefix = '#'
const attributeValuePrefix = ':'

const Util = {
  formatUpdateObject: (data) => {
    let updateExpression = ''
    let expressionAttributeNames = {}
    let expressionAttributeValues = {}

    updateExpression = `set`
    for (let [key, value] of Object.entries(data)) {
      updateExpression += ` ${attributeNamePrefix}${key} = ${attributeValuePrefix}${key},`
      expressionAttributeNames[`${attributeNamePrefix}${key}`] = key
      expressionAttributeValues[`${attributeValuePrefix}${key}`] = value
    }
    updateExpression = updateExpression.substring(0, updateExpression.length - 1)

    return {
      UpdateExpression: updateExpression,
      ExpressionAttributeNames: expressionAttributeNames,
      ExpressionAttributeValues: expressionAttributeValues
    }
  },

  removeTypesFromObject: (object) => {
    const keys = Object.getOwnPropertyNames(object)

    keys.forEach(key => {
      object[key] = (object[key].L) ? object[key].L.map(obj => obj.S)
        : (object[key]['BOOL']) ? object[key].BOOL : object[key].S || object[key].N || object[key]
    })

    return object
  }
}

module.exports = Util
