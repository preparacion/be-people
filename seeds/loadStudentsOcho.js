/**
 * loadGroups.js
 */
const fs = require('fs')
const util = require('util')
const _ = require('underscore')

const Groups = require('../models/groups')
const Students = require('../models/students')
const StudentList = require('../models/studentLists')

const readFile = util.promisify(fs.readFile)

const file = './old_data/lista_ocho.csv'
const dataFile = fs.createWriteStream('./data/lista_ocho.csv', { autoClose: true })

let specialGroups = {
  '1001': '',
  '2002': '',
  '2003': '',
  '3003': ''
}

const lineToJson = function (line) {
  const student = line.split(',')
  const studentData = {
    lastName: student[0],
    secondLastName: student[1],
    name: student[2],
    email: student[3],
    phoneNumber: student[4],
    secondPhoneNumber: student[5],
    '1001': student[6],
    '2002': student[7],
    '2003': student[8],
    '3003': student[9],
    note: student[10],
    cita: student[11],
    pagoCita: student[12],
    groups: []
  }

  for (const i in studentData) {
    if (studentData[i] === '' || studentData[i] === 0 || studentData[i] === '0') {
      delete studentData[i]
    }
  }

  return studentData
}

const listLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

// THIS CODE CAN BE IMPROVED
const saveStudent = async function (line) {
  const data = lineToJson(line)
  const dataSL = {}

  if (data['1001']) {
    dataSL['1001'] = true
    const group = specialGroups['1001']
    const groupId = group.groupId
    data['mate'] = data['1001']
    delete data['1001']

    data.groups = data.groups.concat(groupId)
  }
  if (data['2002']) {
    dataSL['2002'] = true
    const group = specialGroups['2002']
    const groupId = group.groupId
    data['fisica'] = data['2002']
    delete data['2002']

    data.groups = data.groups.concat(groupId)
  }
  if (data['2003']) {
    dataSL['2003'] = true
    const group = specialGroups['2003']
    const groupId = group.groupId
    data['quimica'] = data['2003']
    delete data['2003']

    data.groups = data.groups.concat(groupId)
  }
  if (data['3003']) {
    dataSL['3003'] = true
    const group = specialGroups['3003']
    const groupId = group.groupId
    data['calculo'] = data['3003']
    delete data['3003']

    data.groups = data.groups.concat(groupId)
  }

  // create fields for searching
  if (data['name']) {
    data['name_lower'] = data['name'].toLowerCase()
  }

  if (data['email']) {
    data['email_lower'] = data['email'].toLowerCase()
  }

  if (data['lastName']) {
    data['lastName_lower'] = data['lastName'].toLowerCase()
  }

  const studentResult = await Students.createLoaded(data)
  const studentId = studentResult.student._id

  dataFile.write(studentId + ',' + line + '\n')

  if (dataSL['1001']) {
    const group = specialGroups['1001']
    const studentListId = group.studentListId
    let list = []

    const studentListResult = await StudentList.getById(studentListId)

    if (studentListResult.studentList.list) {
      list = studentListResult.studentList.list
    }
    list = list.concat(studentId)

    await StudentList.update(studentListId, { list })
  }
  if (dataSL['2002']) {

    const group = specialGroups['2002']
    const studentListId = group.studentListId

    let list = []

    const studentListResult = await StudentList.getById(studentListId)

    if (studentListResult.studentList.list) {
      list = studentListResult.studentList.list
    }
    list = list.concat(studentId)

    await StudentList.update(studentListId, { list })
  }
  if (dataSL['2003']) {

    const group = specialGroups['2003']
    const studentListId = group.studentListId
    let list = []

    const studentListResult = await StudentList.getById(studentListId)

    if (studentListResult.studentList.list) {
      list = studentListResult.studentList.list
    }
    list = list.concat(studentId)

    await StudentList.update(studentListId, { list })
  }
  if (dataSL['3003']) {

    const group = specialGroups['3003']
    const studentListId = group.studentListId
    let list = []

    const studentListResult = await StudentList.getById(studentListId)

    if (studentListResult.studentList.list) {
      list = studentListResult.studentList.list
    }
    list = list.concat(studentId)

    await StudentList.update(studentListId, { list })
  }
}

const init = async function () {
  const data = await listLines(file)

  const groupsResult = await Groups.get()

  const allGroups = groupsResult.groups

  allGroups
    .filter(group => group.special)
    .forEach(group => {
      specialGroups[group.oldId] = {
        groupId: group._id,
        studentListId: group.studentListId
      }
    })

  // save header
  dataFile.write('id,' + data.header + '\n')

  await Promise.all(data.lines.map(line => saveStudent(line)))
    .then(() => {})
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })