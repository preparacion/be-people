/**
 * seeds/permissions.js
 *
 * @description :: Creates example node seeds/permissions.js
 * @docs        :: TODO
 */
const Permissions = require('../models/permissions')

const createPermissionAdminstrativo = async function () {
  const adminsData = {
    name: 'Administrativos',
    description: 'Puede agregar Administrativos',
    typePermission: 1
  }
  const result = await Permissions.create(adminsData)
  return result
}

const createPermissionTeachers = async function () {
  const teachersData = {
    name: 'Profesores',
    description: 'Puede agregar Profesores',
    typePermission: 2
  }
  const result = await Permissions.create(teachersData)
  return result
}

const createPermissionAlumnos = async function () {
  const alumnoData = {
    name: 'Alumnos',
    description: 'Puede agregar Alumnos',
    typePermission: 1
  }
  const result = await Permissions.create(alumnoData)
  return result
}

const createSchedule = async function () {
  const cheduleData = {
    name: 'Horarios',
    description: 'Puede agregar Horarios',
    typePermission: 1
  }
  const result = await Permissions.create(cheduleData)
  return result
}

const createSeedsBasics = async function () {
  await createPermissionAdminstrativo()
  await createPermissionTeachers()
  await createPermissionAlumnos()
  await createSchedule()
}

Promise.resolve(createSeedsBasics())
  .then(() => {
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
