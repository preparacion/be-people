/**
 * seeds/regsiter.js
 *
 * @description :: Creates example node seeds/registerd.js
 * @docs        :: TODO
 */

const Register = require('../models/register')
const Roles = require('../models/roles')

const crateUsersAdmin = async function () {
  const superAdminRol = await Roles.findByName('SuperAdmin')
  const userData = {
    name: 'admin1',
    lastmane: 'admin1',
    email: 'admin1@gmail.com',
    password: '123456',
    roleId: superAdminRol.roleId
  }

  const result = await Register.create(userData)
  console.log(result)
}

const crateUsersAdmin2 = async function () {
  const superAdminRol = await Roles.findByName('SuperAdmin')
  const userData = {
    name: 'admin2',
    lastmane: 'admin2',
    email: 'admin2@gmail.com',
    password: '123456',
    roleId: superAdminRol.roleId
  }

  const result = await Register.create(userData)
  console.log(result)
}
const crateUsersAdmin3 = async function () {
  const superAdminRol = await Roles.findByName('SuperAdmin')
  const userData = {
    name: 'admin3',
    lastmane: 'admin3',
    email: 'admin3@gmail.com',
    password: '123456',
    roleId: superAdminRol.roleId
  }

  const result = await Register.create(userData)
  console.log(result)
}

const crateUsersAdmin4 = async function () {
  const superAdminRol = await Roles.findByName('SuperAdmin')
  const userData = {
    name: 'admin4',
    lastmane: 'admin4',
    email: 'admin4@gmail.com',
    password: '123456',
    roleId: superAdminRol.roleId
  }

  const result = await Register.create(userData)
  console.log(result)
}

const createSeeds = async function () {
  await crateUsersAdmin()
  await crateUsersAdmin2()
  await crateUsersAdmin3()
  await crateUsersAdmin4()
}

Promise.resolve(createSeeds())
  .then(() => {
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
