/**
 * seeds/roles.js
 *
 * @description :: Creates example node seeds/roles.js
 * @docs        :: TODO
 */
const Roles = require('../models/roles')
const Permissions = require('../models/permissions')

const superAdminPermissions = [
  'Profesores',
  'Horarios',
  'Administrativos',
  'Alumnos'
]

const teacherPermissions = [
  'Horarios'
]

const adminPermissions = [
  'Profesores',
  'Alumnos'
]

const createRolAdminstrativo = async function () {
  const permissions = await Promise.all(superAdminPermissions.map(permission => Permissions.findByName(permission)))
    .then(permissions => permissions)

  const superUserData = {
    name: 'SuperAdmin',
    permissions: permissions.map(permission => permission.permissionId)
  }
  const result = await Roles.create(superUserData)
  return result
}

const createRolProfesor = async function () {
  const permissions = await Promise.all(teacherPermissions.map(permission => Permissions.findByName(permission)))
    .then(permissions => permissions)

  const profesorData = {
    name: 'Profesor',
    permissions: permissions.map(permission => permission.permissionId)
  }
  const result = await Roles.create(profesorData)
  return result
}

const createRolAdministrativo = async function () {
  const permissions = await Promise.all(adminPermissions.map(permission => Permissions.findByName(permission)))
    .then(permissions => permissions)

  const adminData = {
    name: 'Administrativo',
    permissions: permissions.map(permission => permission.permissionId)
  }
  const result = await Roles.create(adminData)
  return result
}

const createRolAlumno = async function () {
  const alumnoData = {
    name: 'Alumnos',
    permissions: []
  }
  const result = await Roles.create(alumnoData)
  return result
}

const createSeedsBasics = async function () {
  await createRolAdminstrativo()
  await createRolProfesor()
  await createRolAlumno()
  await createRolAdministrativo()
}

Promise.resolve(createSeedsBasics())
  .then(() => {
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
