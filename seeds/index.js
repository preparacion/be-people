/**
 * seeds/index.js
 *
 * @description :: Create seeds
 * @docs        :: TODO
 */
async function createSeeds () {
  await require('./permissions')
  await require('./roles')
  await require('./register')
}
setTimeout(async () => {
  await createSeeds()
}, 1000)
