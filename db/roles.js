/**
 * db/roles.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const RoleSchema = new Schema({
  name: { type: String, required: true },
  permissions: [{ type: Schema.Types.ObjectId, ref: 'Permissions' }]
}, { versionKey: false })

RoleSchema.virtual('id').get(() => {
  return this._id
})

const Roles = db.model('Roles', RoleSchema, 'Roles')

module.exports = Roles
