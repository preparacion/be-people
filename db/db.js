/**
 * db/db.js
 *
 * @description :: Describes the db mongoose.
 * @docs        :: https://mongoosejs.com/
 */
const mongoose = require('mongoose')

const { mongodbURI } = require('../config')

mongoose.connect(mongodbURI)

const connection = mongoose.connection
connection.on('error', console.error.bind(console, 'connection error:'))
connection.once('open', function () {
  // we're connected!
  console.log('connected')
})

module.exports = mongoose
