/**
 * db/permissions.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const PermissionSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  type: { type: Number }
}, { versionKey: false })

PermissionSchema.virtual('id').get(() => {
  return this._id
})

const Permissions = db.model('Permissions', PermissionSchema, 'Permissions')

module.exports = Permissions
