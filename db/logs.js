/**
 * db/logs.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const LogSchema = new Schema({
  log: { type: String },
  date: { type: Date, default: new Date() },
  userId: { type: Schema.Types.ObjectId, ref: 'Users' }
}, { versionKey: false })

LogSchema.virtual('id').get(() => {
  return this._id
})

const Logs = db.model('Logs', LogSchema, 'Logs')

module.exports = Logs
