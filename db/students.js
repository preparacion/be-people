/**
 * db/students.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const StudentSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  lastName: { type: String },
  secondLastName: { type: String },
  email: { type: String },
  password: { type: String },
  birthDate: { type: Date },
  address: { type: String },
  postalCode: { type: String },
  name_lower: { type: String },
  email_lower: { type: String },
  lastName_lower: { type: String },
  phoneNumber: { type: String },
  roleId: { type: Schema.Types.ObjectId, ref: 'Roles' }
}, { versionKey: false })

StudentSchema.virtual('id').get(() => {
  return this._id
})

const Students = db.model('Students', StudentSchema, 'Students')

module.exports = Students
