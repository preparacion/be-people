/**
 * db/users.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const UserSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  lastName: { type: String },
  secondLastName: { type: String },
  email: { type: String },
  password: { type: String },
  birthDate: { type: Date },
  address: { type: String },
  postalCode: { type: String },
  phoneNumber: { type: String },
  roleId: { type: Schema.Types.ObjectId, ref: 'Roles' }
}, { versionKey: false })

UserSchema.virtual('id').get(() => {
  return this._id
})

const Users = db.model('Users', UserSchema, 'Users')

module.exports = Users
