/**
 * db/studentLists.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const StudentListSchema = new Schema({
  name: { type: String, required: true },
  list: [{ type: Schema.Types.ObjectId, ref: 'Students' }]
}, { versionKey: false })

StudentListSchema.virtual('id').get(() => {
  return this._id
})

const StudentLists = db.model('StudentLists', StudentListSchema, 'StudentLists')

module.exports = StudentLists
