/**
 * schemas/pagination.js
 *
 * @description :: Defines pagination validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
    .default(null)
})

module.exports = { paginationSchema }
