/**
 * schemas/payment.js
 *
 * @description :: Defines payment validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const paymentSchema = Joi.object().keys({
  paymentType: Joi.string().valid(['cash', 'card']).required(),
  amount: Joi.number().required(),
  cardNumber: Joi.string().creditCard(),
  expirationDate: Joi.string().length(4),
  cardType: Joi.string().valid(['credit', 'debit']),
  cvv: Joi.string().length(3)
})

module.exports = {
  paymentSchema
}
