/**
 * schemas/student.js
 *
 * @description :: Defines student validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const studentSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    lastName: Joi.string().min(1).max(100).required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().min(4).max(100),
    secondLastName: Joi.string().min(1).max(100),
    birthDate: Joi.date().iso(),
    phoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    registerType: Joi.string()
})

const updateStudentSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100),
    lastName: Joi.string().min(1).max(100),
    email: Joi.string().email({ minDomainAtoms: 2 }),
    password: Joi.string().min(4).max(100),
    secondLastName: Joi.string().min(1).max(100),
    birthDate: Joi.date(),
    phoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    registerType: Joi.string()
  })

module.exports = {
  studentSchema,
  updateStudentSchema
}
