/**
 * schemas/group.js
 *
 * @description :: Defines group validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

// used in student registration
const groupIdSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
    .required()
})

module.exports = {
  groupIdSchema
}
