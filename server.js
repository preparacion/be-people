/**
 * server.js
 *
 * @description :: Loads server configuration
 */
const Koa = require('koa')
const cors = require('koa-cors')
const bodyParser = require('koa-bodyparser')
const app = new Koa()

const routes = require('./routes')

// Cors
app.use(cors())

app.use(bodyParser({
  enableTypes: ['json', 'form'],
  formLimit: '10mb',
  jsonLimit: '10mb'
}))

// routes
app.use(routes.routes(), routes.allowedMethods())
// log activities
const logs = require('./middleware/createLogs')
app.use(logs)

module.exports = app
