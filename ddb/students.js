/**
 * ddb/students.js
 *
 * @description :: Describes the students db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'students'

const userSchema = {
  TableName: 'students',
  KeySchema: [
    { AttributeName: 'studentId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'studentId', AttributeType: 'S' },
    { AttributeName: 'email', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 30,
    WriteCapacityUnits: 30
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'EmailIndex',
      KeySchema: [
        { AttributeName: 'email', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 20,
        WriteCapacityUnits: 20
      }
    }
  ]
}

const createStudentsTable = async () => {
  const createTablePromise = ddbClient.createTable(userSchema).promise()
  await createTablePromise
    .then(result => {
      console.log('+ Students table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Students table.', JSON.stringify(err, null, 2))
    })
}

const deleteStudentsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  await deleteTablePromise
    .then(result => {
      console.log('+ Students table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Students table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createStudentsTable, deleteStudentsTable }
