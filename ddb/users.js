/**
 * ddb/users.js
 *
 * @description :: Describes the users db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'users'

const userSchema = {
  TableName: 'users',
  KeySchema: [
    { AttributeName: 'userId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'userId', AttributeType: 'S' },
    { AttributeName: 'email', AttributeType: 'S' },
    { AttributeName: 'phoneNumber', AttributeType: 'S' },
    { AttributeName: 'roleId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 10,
    WriteCapacityUnits: 10
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'EmailIndex',
      KeySchema: [
        { AttributeName: 'email', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    },
    {
      IndexName: 'PhoneIndex',
      KeySchema: [
        { AttributeName: 'phoneNumber', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    },
    {
      IndexName: 'RoleIndex',
      KeySchema: [
        { AttributeName: 'roleId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createUsersTable = async () => {
  const createTablePromise = ddbClient.createTable(userSchema).promise()
  await createTablePromise
    .then(result => {
      console.log('+ Users table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create users table.', JSON.stringify(err, null, 2))
    })
}

const deleteUsersTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  await deleteTablePromise
    .then(result => {
      console.log('+ Users table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Users table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createUsersTable, deleteUsersTable }
