/**
 * ddb/logs.js
 *
 * @description :: Describes the logs db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'logs'

const logSchema = {
  TableName: 'logs',
  KeySchema: [
    { AttributeName: 'logId', KeyType: 'HASH' },
    { AttributeName: 'date', KeyType: 'RANGE' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'logId', AttributeType: 'S' },
    { AttributeName: 'userId', AttributeType: 'S' },
    { AttributeName: 'date', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'UserIndex',
      KeySchema: [
        { AttributeName: 'userId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createLogsTable = async () => {
  const createTablePromise = ddbClient.createTable(logSchema).promise()
  await createTablePromise
    .then(result => {
      console.log('+ Logs table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create logs table.', JSON.stringify(err, null, 2))
    })
}

const deleteLogsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  await deleteTablePromise
    .then(result => {
      console.log('+ Logs table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Logs table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createLogsTable, deleteLogsTable }
