/**
 * ddb/createTables.js
 *
 * @description :: Creates tables
 * @docs        :: TODO
 */
const { createUsersTable } = require('./users')
const { createStudentsTable } = require('./students')
const { createRolesTable } = require('./roles')
const { createPermissionsTable } = require('./permissions')
const { createLogsTable } = require('./logs')

async function createTables () {
  await createUsersTable()
  await createStudentsTable()
  await createPermissionsTable()
  await createRolesTable()
  await createLogsTable()
}

Promise.resolve(createTables())
  .then(() => {
    console.log('Tables created...')
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })

