/**
 * ddb/permissions.js
 *
 * @description :: Describes the permissions db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'permissions'

const permissionsSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'permissionId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'permissionId', AttributeType: 'S' },
    { AttributeName: 'name', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'NameIndex',
      KeySchema: [
        { AttributeName: 'name', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createPermissionsTable = async () => {
  const createTablePromise = ddbClient.createTable(permissionsSchema).promise()
  await createTablePromise
    .then(result => {
      console.log('+ Permissions table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Permissions table.', JSON.stringify(err, null, 2))
    })
}

const deletePermissionsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  await deleteTablePromise
    .then(result => {
      console.log('+ Permissions table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Permissions table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createPermissionsTable, deletePermissionsTable }
