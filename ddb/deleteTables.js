/**
 * ddb/createTables.js
 *
 * @description :: Creates tables
 * @docs        :: TODO
 */
const { deleteUsersTable } = require('./users')
const { deleteStudentsTable } = require('./students')
const { deleteRolesTable } = require('./roles')
const { deletePermissionsTable } = require('./permissions')
const { deleteLogsTable } = require('./logs')

async function deleteTables () {
  await deleteUsersTable()
  await deleteStudentsTable()
  await deleteRolesTable()
  await deletePermissionsTable()
  await deleteLogsTable()
}

Promise.resolve(deleteTables())
  .then(() => {
    console.log('Tables deleted...')
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
