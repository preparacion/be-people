/**
 * ddb/roles.js
 *
 * @description :: Describes the roles db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'roles'

const rolesSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'roleId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'roleId', AttributeType: 'S' },
    { AttributeName: 'name', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'NameIndex',
      KeySchema: [
        { AttributeName: 'name', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createRolesTable = async () => {
  const createTablePromise = ddbClient.createTable(rolesSchema).promise()
  await createTablePromise
    .then(result => {
      console.log('+ Roles table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Roles table.', JSON.stringify(err, null, 2))
    })
}

const deleteRolesTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  await deleteTablePromise
    .then(result => {
      console.log('+ Roles table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Permissions table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createRolesTable, deleteRolesTable }
