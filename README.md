# be-base

## 1. Create .envs directory and add the configuration files

For example: **development**

```
NODE_ENV=development
APP_PORT=3100
```

## 2. Run a local dynamoDB instance

Download a .zip version from

```
https://docs.aws.amazon.com/es_es/amazondynamodb/latest/developerguide/DynamoDBLocal.html
```

Unzip and run:

```
java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb
```

Probably, you must install _aws cli_:

```
https://aws.amazon.com/es/cli/
```

In a new terminal:

```
aws configure
```

Type:

```
AWS Access Key ID [None]: foo
AWS Secret Access Key [None]: bar
Default region name [None]: local
Default output format [None]: json
```

## 3. Create Tables

Run

```
npm run createDB
```

## 4. Delete Tables

Run

```
npm run deleteDB
```
