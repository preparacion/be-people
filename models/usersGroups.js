/**
 * models/usersGroups.js
 *
 * @description :: Describes the users functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const Util = require('../util')
const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'usersGroups'

const UsersGroups = {
  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - UsersGroups id.
   */
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        usersGroupsId: {
          S: id
        }
      }
    }

    const getUserGroup = ddbClient.getItem(params).promise()
    const userGroup = await getUserGroup.then(userGroup => userGroup)
      .catch(err => {
        console.error('- Error trying to get a userGroup /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(userGroup)) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${id}`
      }
    }

    return {
      success: true,
      userGroup
    }
  },

  getFromUser: async userId => {
    const params = {
      TableName: tableName,
      IndexName: 'UserIndex',
      KeyConditionExpression: 'userId = :userId',
      ExpressionAttributeValues: {
        ':userId': userId
      }
    }

    const getUsersGroups = docClient.query(params).promise()
    const result = await getUsersGroups
      .then(result => result.Items)
      .catch(err => {
        console.error('- Error trying to get an user (findUserByEmail).', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return (result.length) ? { success: true, usersGroups: result }
      : { success: false, code: 404, message: `UsersGroup not found for this user: ${userId}` }
  }
}

module.exports = UsersGroups
