/**
 * models/users.js
 *
 * @description :: Describes the users functions
 * @docs        :: TODO
 */
const UserModel = require('../db/users')

const Users = {
  findByPhone: async phoneNumber => {
    // TODO
  },

  findByEmail: async (email = '') => {
    // TODO
  },

  findByRoleId: async roleId => {
    // TODO
  },

  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const users = await UserModel
      .find({})
      .then(users => {
        return {
          success: true,
          users
        }
      })
      .catch(err => {
        console.error('- Error trying to get all users.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return users
  },

  /**
   * getById
   *
   * @description: Return a single item by id
   * @param id {string} - item id
   */
  getById: async id => {
    const user = await UserModel.findOne({ _id: id })
      .then(user => {
        return {
          success: true,
          user
        }
      })
      .catch(err => {
        console.error('- Error trying to get a user by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return user
  },

  /**
   * create
   *
   * @@description: Creates a new Item
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const user = new UserModel(data)
    const result = await user.save()
      .then(user => {
        return {
          success: true,
          user
        }
      })
      .catch(err => {
        console.error('- Error trying to create a user', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * update
   *
   * @description: Update a single item by id.
   * @param {string} id - Permission id.
   * @param {object} data - List of properties.
   */
  update: async (id, data) => {
    const user = await UserModel.getById(id)

    if (!user.success) {
      return {
        success: false,
        code: 404,
        message: `Permission not found ${id}`
      }
    }

    const result = await UserModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      })
      .then(user => {
        return {
          success: true,
          user
        }
      })
      .catch(err => {
        console.error('- Error trying to update a user', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Permission id.
   */
  delete: async id => {
    const result = await UserModel.deleteOne({ _id: id })
      .then(() => {
        return {
          success: true,
          deleted: true
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a user', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * getGroups
   *
   * @description: Gets the user's groups
   * @param {string} id - User id.
   */
  getGroups: async id => {
    // TODO
  }
}

module.exports = Users
