/**
 * models/login.js
 *
 * @description :: Describes the login functions
 * @docs        :: TODO
 */
const bcrypt = require('bcryptjs')

const Users = require('./users')

const Login = {
  exec: async (email, password = '') => {
    const user = await Users.findByEmail(email)

    if (!user) {
      return {
        success: false,
        code: 401,
        error: 'Invalid email or password'
      }
    }

    const hashedPass = user.hashed_password || ''

    if (bcrypt.compareSync(password, hashedPass)) {
      if (user.hashed_password) {
        delete user.hashed_password
      }
      return {
        success: true,
        message: 'Logged',
        user
      }
    } else {
      return {
        success: false,
        code: 401,
        error: 'Invalid email or password'
      }
    }
  }
}

module.exports = Login
