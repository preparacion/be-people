/**
 * models/roles.js
 *
 * @description :: Describes the roles functions
 * @docs        :: TODO
 */
const RoleModel = require('../db/roles')

const Roles = {
  /**
   * getById
   *
   * @description: Return a single item by id
   * @param id {string} - item id
   */
  getById: async id => {
    const role = await RoleModel.findOne({ _id: id })
      .then(role => {
        return {
          success: true,
          role
        }
      })
      .catch(err => {
        console.error('- Error trying to get a role by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return role
  },

  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const roles = await RoleModel
      .find({})
      .then(roles => {
        return {
          success: true,
          roles
        }
      })
      .catch(err => {
        console.error('- Error trying to get all roles.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return roles
  },

  findByName: async name => {
    // TODO
  },

  /**
   * create
   *
   * @@description: Creates a new Item
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const role = new RoleModel(data)
    const result = await role.save()
      .then(role => {
        return {
          success: true,
          role
        }
      })
      .catch(err => {
        console.error('- Error trying to create a role', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  updatePermissions: async (id, roles) => {
    // TODO ?
    // it can be done using update method
  },

  /**
   * update
   *
   * @description: Update a single item by id.
   * @param {string} id - Item id.
   * @param {object} data - List of properties.
   */
  update: async (id, data) => {
    const role = await RoleModel.getById(id)

    if (!role.success) {
      return {
        success: false,
        code: 404,
        message: `Role not found ${id}`
      }
    }

    const result = await RoleModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      })
      .then(role => {
        return {
          success: true,
          role
        }
      })
      .catch(err => {
        console.error('- Error trying to update a role', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Permission id.
   */
  delete: async id => {
    const result = await RoleModel.deleteOne({ _id: id })
      .then(() => {
        return {
          success: true,
          deleted: true
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a role', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * getPermissions
   *
   * @description: Returns the roles by its id
   * @param {Array} permissionsIds - List of ids
   */
  getPermissions: async permissionsIds => {
    // TODO
    // const roles = await Promise
    //   .all(
    //     permissionsIds.map(id => {
    //       return Permissions.getById(id)
    //     })
    //   )
    //   .then(results => {
    //     return results.map(result => result.role.Item)
    //   })
    //   .catch(err => {
    //     console.error(`Error trying to get roles ${JSON.stringify(err, null, 2)}`)
    //     return false
    //   })

    // return roles
  },

  getUsers: async id => {
    // TODO
    //   const role = await Roles.getById(id)

    //   if (!role.success) {
    //     return {
    //       success: false,
    //       code: 404,
    //       error: `Role not found: ${id}`
    //     }
    //   }

    //   const users = await Users.findByRoleId(id)

    //   if (!users) {
    //     return {
    //       success: false,
    //       code: 404,
    //       error: `Users not found for this id: ${id}`
    //     }
    //   }

    //   return {
    //     success: true,
    //     users
    //   }
  }
}

module.exports = Roles
