/**
 * models/locations.js
 *
 * @description :: Describes the locations functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const Utils = require('../util')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'locations'

const Locations = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const getLocations = ddbClient.scan({ TableName: tableName }).promise()
    const locations = await getLocations
      .then(locations => locations.Items)
      .catch(err => {
        console.error('- Error trying to get all locations.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      locations
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Location id.
   */
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        locationId: {
          S: id
        }
      }
    }

    const getLocation = ddbClient.getItem(params).promise()
    const location = await getLocation.then(location => location)
      .catch(err => {
        console.error('- Error trying to get a location /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(location)) {
      return {
        success: false,
        code: 404,
        error: `Location not found: ${id}`
      }
    }

    return {
      success: true,
      location: Utils.removeTypesFromObject(location.Item)
    }
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        locationId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createLocation = docClient.put(params).promise()

    const result = await createLocation
      .then(() => {
        return {
          success: true,
          locationId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new location.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Locations
