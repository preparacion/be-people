/**
 * models/payments.js
 *
 * @description :: Describes the payments functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'payments'

const Payments = {
  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        paymentId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createLocation = docClient.put(params).promise()

    const result = await createLocation
      .then(() => {
        return {
          success: true,
          paymentId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new payment.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Payments
