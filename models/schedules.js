/**
 * models/schedules.js
 *
 * @description :: Describes the schedules functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'schedules'

const Schedules = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const getSchedules = ddbClient.scan({ TableName: tableName }).promise()
    const schedules = await getSchedules
      .then(schedules => schedules.Items)
      .catch(err => {
        console.error('- Error trying to get all schedules.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      schedules
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Schedule id.
   */
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        scheduleId: {
          S: id
        }
      }
    }

    const getSchedule = ddbClient.getItem(params).promise()
    const schedule = await getSchedule.then(schedule => schedule)
      .catch(err => {
        console.error('- Error trying to get a schedule /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(schedule)) {
      return {
        success: false,
        code: 404,
        error: `Location not found: ${id}`
      }
    }

    return {
      success: true,
      schedule
    }
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        scheduleId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createSchedule = docClient.put(params).promise()

    const result = await createSchedule
      .then(() => {
        return {
          success: true,
          scheduleId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new schedule.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Schedules
