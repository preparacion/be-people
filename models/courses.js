/**
 * models/courses.js
 *
 * @description :: Describes the courses functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'courses'

const Courses = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const getCourses = ddbClient.scan({ TableName: tableName }).promise()
    const courses = await getCourses
      .then(courses => {
        return courses.Items
      })
      .catch(err => {
        console.error('- Error trying to get all courses.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      courses
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Course id.
   */
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        courseId: {
          S: id
        }
      }
    }

    const getCourse = ddbClient.getItem(params).promise()
    const course = await getCourse.then(course => course)
      .catch(err => {
        console.error('- Error trying to get a course /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(course)) {
      return {
        success: false,
        code: 404,
        error: `Course not found: ${id}`
      }
    }

    return {
      success: true,
      course
    }
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        courseId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createCourse = docClient.put(params).promise()

    const result = await createCourse
      .then(() => {
        return {
          success: true,
          courseId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new course.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Courses
