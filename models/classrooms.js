/**
 * models/classRooms.js
 *
 * @description :: Describes the classRooms functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const uuidv4 = require('uuid/v4')

const { ddbClient, docClient } = require('../ddb/client')

const tableName = 'classrooms'

const ClassRooms = {
  /**
   * get
   *
   * @description: Returns all the items
   * @returns {array} - List of items
   */
  get: async () => {
    const getClassRooms = ddbClient.scan({ TableName: tableName }).promise()
    const classRooms = await getClassRooms
      .then(classRooms => classRooms.Items)
      .catch(err => {
        console.error('- Error trying to get the class rooms.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      classRooms
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Location id.
   */
  getById: async id => {
    const params = {
      TableName: tableName,
      Key: {
        classRoomId: {
          S: id
        }
      }
    }

    const getClassRoom = ddbClient.getItem(params).promise()
    const classRoom = await getClassRoom.then(classRoom => classRoom)
      .catch(err => {
        console.error('- Error trying to get a classRoom /:id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (_.isEmpty(classRoom)) {
      return {
        success: false,
        code: 404,
        error: `classRoom not found: ${id}`
      }
    }

    return {
      success: true,
      classRoom
    }
  },

  /**
   * create
   *
   * @description: Returns all the items.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const id = uuidv4()

    let params = {
      TableName: tableName,
      Item: {
        classRoomId: id
      }
    }

    params.Item = _.extend(params.Item, data)

    const createClassRoom = docClient.put(params).promise()

    const result = await createClassRoom
      .then(() => {
        return {
          success: true,
          classRoomId: id
        }
      })
      .catch(err => {
        console.error('- Error trying to create a new class room.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = ClassRooms
