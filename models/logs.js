/**
 * models/logs.js
 *
 * @description :: Describes the logs functions
 * @docs        :: TODO
 */
const LogModel = require('../db/logs')

const Logs = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const logs = await LogModel
      .find({})
      .then(logs => {
        return {
          success: true,
          logs
        }
      })
      .catch(err => {
        console.error('- Error trying to get all logs.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return logs
  },

  /**
   * getById
   *
   * @description: Return a single item by id
   * @param id {string} - item id
   */
  getById: async id => {
    const log = await LogModel.findOne({ _id: id })
      .then(log => {
        return {
          success: true,
          log
        }
      })
      .catch(err => {
        console.error('- Error trying to get a log by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return log
  },

  getByUserId: async userId => {
    // TODO
  },

  /**
   * create
   *
   * @@description: Creates a new Item
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const log = new LogModel(data)
    const result = await log.save()
      .then(log => {
        return {
          success: true,
          log
        }
      })
      .catch(err => {
        console.error('- Error trying to create a log', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Logs
