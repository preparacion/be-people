/**
 * models/students.js
 *
 * @description :: Describes the students functions
 * @docs        :: TODO
 */
const bcrypt = require('bcryptjs')

const StudentModel = require('../db/students')

const Groups = require('./groups')
const Credits = require('./credits')
const Payments = require('./payments')
const PaymentInfo = require('./paymentInfo')
const UsersGroups = require('./usersGroups')
const StudentLists = require('./studentLists')

const tableName = 'students'

const Students = {
  find: async (term = '') => {
    console.log(term)
    const params = {
      TableName: tableName,
      FilterExpression:
        `begins_with(#email, :term) or 
        begins_with(#name, :term) or
        begins_with(#lastName, :term) or
        begins_with(#email_lower, :term) or
        begins_with(#name_lower, :term) or
        begins_with(#lastName_lower, :term) or
        begins_with(#phoneNumber, :term) or
        begins_with(#secondPhoneNumber, :term)`,
      ExpressionAttributeNames: {
        '#email': 'email',
        '#name': 'name',
        '#lastName': 'lastName',
        '#email_lower': 'email_lower',
        '#name_lower': 'name_lower',
        '#lastName_lower': 'lastName_lower',
        '#phoneNumber': 'phoneNumber',
        '#secondPhoneNumber': 'secondPhoneNumber'
      },
      ExpressionAttributeValues: {
        ':term': term
      }
    }

    const getStudent = docClient.scan(params).promise()
    const result = await getStudent
      .then(result => {
        console.log('.........')
        console.log(result)
        console.log('.........')
        return {
          success: true,
          students: result.Items
        }
      })
      .catch(err => {
        console.error(`- Error trying to find a student.`, JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },

  findByEmail: async (email = '') => {
    const student = await StudentModel
      .findOne({ email: email })
      .then(student => {
        return {
          success: true,
          student
        }
      })
      .catch(err => {
        console.error('- Error trying to get a student by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return student
  },

  get: async ({ skip, limit } = {}) => {
    const students = await StudentModel
      .find({})
      .skip(skip)
      .limit(limit)
      .then(students => {
        return {
          success: true,
          students
        }
      })
      .catch(err => {
        console.error('- Error trying to get all students.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return students
  },

  /**
   * getById
   *
   * @description: Return a single item by id
   * @param id {string} - item id
   */
  getById: async id => {

  },

  register: async data => {
    // let studentId, paymentId, creditId, paymentInfoId
    // data.student.groups = []

    // // create fields for searching
    // if (data.student['name']) {
    //   data.student['name_lower'] = data.student['name'].toLowerCase()
    // }

    // if (data.student['email']) {
    //   data.student['email_lower'] = data.student['email'].toLowerCase()
    // }

    // if (data.student['lastName']) {
    //   data.student['lastName_lower'] = data.student['lastName'].toLowerCase()
    // }

    // const student = await Students.create(data.student)

    // if (!student.success) {
    //   // return error
    //   return student
    // }

    // studentId = student.studentId

    // if (data.group) {
    //   const group = await Groups.getById(data.group.id)

    //   if (!group.success) {
    //     // return error
    //     return group
    //   }

    //   const studentListId = group.group.Item.studentListId.S
    //   let list = []

    //   list.push(data.group.id)

    //   await Students.update(studentId, { groups: list })

    //   const studentList = await StudentLists.getById(studentListId)

    //   let slList = studentList.studentList.list

    //   slList.push(studentId)

    //   await StudentLists.update(studentListId, { list: slList })
    // }

    // if (data.payment) {
    //   data.payment['studentId'] = studentId
    //   data.payment['date'] = new Date().toISOString()

    //   if (data.payment['paymentType'] === 'card') {
    //     const paymentInfo = await PaymentInfo.create({
    //       cardNumber: data.payment['cardNumber'],
    //       expirationDate: data.payment['expirationDate'],
    //       cardType: data.payment['cardType'],
    //       cvv: data.payment['cvv']
    //     })

    //     if (!paymentInfo.success) {
    //       // return error
    //       return paymentInfo
    //     }

    //     paymentInfoId = paymentInfo.paymentInfoId
    //   }

    //   const payment = await Payments.create(data.payment)

    //   if (!payment.success) {
    //     // return error
    //     return payment
    //   }

    //   paymentId = payment.paymentId

    //   const credit = await Credits.create({
    //     amount: data.payment.amount,
    //     date: new Date().toISOString(),
    //     studentId
    //   })
  
    //   if (!credit.success) {
    //     // return error
    //     return credit
    //   }
  
    //   creditId = credit.creditId
    // }

    // return {
    //   success: true,
    //   studentId,
    //   paymentId,
    //   creditId,
    //   paymentInfoId
    // }
  },

  /**
   * create
   *
   * @@description: Creates a new Item
   * @param {object} data - List of attributes.
   */
  create: async data => {
    // Search by email
    if (data.email) {
      const student = await Students.findByEmail(data.email)
      if (student.success) {
        return {
          success: false,
          code: 400,
          error: `Student already registered ${data.email}`
        }
      }
    }

    if (data.password) {
      data.hashed_password = await bcrypt.hash(data.password, 10)
      delete data.password
    }

    // create fields for searching
    if (data['name']) {
      data['name_lower'] = data['name'].toLowerCase()
    }

    if (data['email']) {
      data['email_lower'] = data['email'].toLowerCase()
    }

    if (data['lastName']) {
      data['lastName_lower'] = data['lastName'].toLowerCase()
    }

    const student = new StudentModel(data)
    const result = await student.save()
      .then(student => {
        return {
          success: true,
          student
        }
      })
      .catch(err => {
        console.error('- Error trying to create a student', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  // used in migration process only
  createLoaded: async data => {
    if (data.password) {
      data.hashed_password = await bcrypt.hash(data.password, 10)
      delete data.password
    }

    const student = new StudentModel(data)
    const result = await student.save()
      .then(student => {
        return {
          success: true,
          student
        }
      })
      .catch(err => {
        console.error('- Error trying to create a student', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  update: async (id, data) => {
    const student = await Students.getById(id)

    if (!student.success) {
      return {
        success: false,
        code: 404,
        error: `User not found: ${id}`
      }
    }

    // create fields for searching
    if (data['name']) {
      data['name_lower'] = data['name'].toLowerCase()
    }

    if (data['email']) {
      data['email_lower'] = data['email'].toLowerCase()
    }

    if (data['lastName']) {
      data['lastName_lower'] = data['lastName'].toLowerCase()
    }

    const result = await StudentModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      })
      .then(student => {
        return {
          success: true,
          student
        }
      })
      .catch(err => {
        console.error('- Error trying to update a student', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  delete: async id => {
    const result = await StudentModel.deleteOne({ _id: id })
      .then(() => {
        return {
          success: true,
          deleted: true
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a student', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * getGroups
   *
   * @description: Gets the student's groups
   * @param {string} id - User id.
   */
  getGroups: async id => {
    // const student = await Students.getById(id)

    // if (!student.success) {
    //   return {
    //     code: 404,
    //     success: false,
    //     error: `User not found: ${id}`
    //   }
    // }

    // const usersGroups = await UsersGroups.getFromUser(id)
    // if (!usersGroups.success) {
    //   return {
    //     success: false,
    //     code: 404,
    //     error: `No groups for this student: ${id}`
    //   }
    // }

    // const groupsIds = usersGroups.usersGroups.map(userGroup => userGroup.groupId)
    // const groups = await Promise.all(groupsIds.map(groupId => Groups.getById(groupId)))
    //   .then(result => {
    //     return result.map(group => group.group.Item)
    //   })
    //   .catch(err => {
    //     console.error('- Error trying to get the groups from an student.', JSON.stringify(err, null, 2))
    //     return {
    //       success: false,
    //       code: 500,
    //       error: JSON.stringify(err, null, 2)
    //     }
    //   })

    // return {
    //   success: true,
    //   groups
    // }
  }
}

module.exports = Students
