/**
 * models/permissions.js
 *
 * @description :: Describes the permissions functions
 * @docs        :: TODO
 */
const PermissionModel = require('../db/permissions')
const Permissions = {
  /**
   * findByName
   *
   * @description: Get permission by name
   * @param {string} name - Permission name.
   */
  // findByName: async name => {
  //   const params = {
  //     TableName: tableName,
  //     IndexName: 'NameIndex',
  //     KeyConditionExpression: '#name = :name',
  //     ExpressionAttributeNames: {
  //       '#name': 'name'
  //     },
  //     ExpressionAttributeValues: {
  //       ':name': name
  //     }
  //   }

  //   const getPermission = docClient.query(params).promise()
  //   const result = await getPermission
  //     .then(result => result.Items)
  //     .catch(err => {
  //       console.error('- Error trying to get a permission (findByName).', JSON.stringify(err, null, 2))
  //       return {
  //         success: false,
  //         code: 500,
  //         error: JSON.stringify(err, null, 2)
  //       }
  //     })

  //   return (result.length) ? result[0] : false
  // },

  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const permissions = await PermissionModel
      .find({})
      .then(permissions => {
        return {
          success: true,
          permissions
        }
      })
      .catch(err => {
        console.error('- Error trying to get all permissions.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return permissions
  },

  /**
   * getById
   *
   * @description: Return a single item by id
   * @param id {string} - item id
   */
  getById: async id => {
    const permission = await PermissionModel.findOne({ _id: id })
      .then(permission => {
        return {
          success: true,
          permission
        }
      })
      .catch(err => {
        console.error('- Error trying to get a permission by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return permission
  },

  /**
   * create
   *
   * @@description: Creates a new Item
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const permission = new PermissionModel(data)
    const result = await permission.save()
      .then(permission => {
        return {
          success: true,
          permission
        }
      })
      .catch(err => {
        console.error('- Error trying to create a permission', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * update
   *
   * @description: Update a single item by id.
   * @param {string} id - Permission id.
   * @param {object} data - List of properties.
   */
  update: async (id, data) => {
    const permission = await PermissionModel.getById(id)

    if (!permission.success) {
      return {
        success: false,
        code: 404,
        message: `Permission not found ${id}`
      }
    }

    const result = await PermissionModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      })
      .then(permission => {
        return {
          success: true,
          permission
        }
      })
      .catch(err => {
        console.error('- Error trying to update a permission', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Permission id.
   */
  delete: async id => {
    const result = await PermissionModel.deleteOne({ _id: id })
      .then(() => {
        return {
          success: true,
          deleted: true
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a permission', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Permissions
