/**
 * models/groups.js
 *
 * @description :: Describes the groups functions
 * @docs        :: TODO
 */
const GroupModel = require('../db/groups')

const Groups = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const groups = await GroupModel
      .find({})
      .then(groups => {
        return {
          success: true,
          groups
        }
      })
      .catch(err => {
        console.error('- Error trying to get all groups.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return groups
  },
  /**
   * getById
   *
   * @description: Returns all the items
   * @param id {string} - Group id
   */
  getById: async id => {
    const group = await GroupModel.findOne({ _id: id })
      .then(group => {
        return {
          success: true,
          group
        }
      })
      .catch(err => {
        console.error('- Error trying to get a group by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return group
  },

  getAvailable: async group => {
    // TODO
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const group = new GroupModel(data)
    const result = await group.save()
      .then(group => {
        return {
          success: true,
          group
        }
      })
      .catch(err => {
        console.error('- Error trying to create a group', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * update
   *
   * @description: update an item.
   * @param {string} id - Item id
   * @param {object} data - List of attributes.
   */
  update: async (id, data) => {
    const group = await Groups.getById(id)

    if (!group.success) {
      return {
        success: false,
        code: 404,
        message: `Group not found ${id}`
      }
    }

    const result = await GroupModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      })
      .then(group => {
        return {
          success: true,
          group
        }
      })
      .catch(err => {
        console.error('- Error trying to update a group', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  enroll: async (id, studentId, data, user) => {
    // TODO
  },

  delete: async id => {
    const result = await GroupModel.deleteOne({ _id: id })
      .then(() => {
        return {
          success: true,
          deleted: true
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a group', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  getByCourseId: async courseId => {
    // TODO
  }
  // getAvailable: async group => {
  //   const capacity = group.capacity
  //   const studentListId = group.studentListId
  //   const studentListResult = await StudentList.getById(studentListId)
  //   const studentList = Util.removeTypesFromObject(studentListResult.studentList.Item)
  //   const enrolled = studentList.list.length
  //   group['available'] = capacity - enrolled

  //   return group
  // },

  // enroll: async (id, studentId, data, user) => {
  //   let paymentInfoId, paymentId, creditId
  //   const studentResult = await Students.getById(studentId)

  //   if (!studentResult.success) {
  //     return {
  //       success: false,
  //       code: 400,
  //       error: 'Student not found'
  //     }
  //   }

  //   const student = studentResult.student

  //   const groupResult = await Groups.getById(id)

  //   if (!groupResult.success) {
  //     return {
  //       success: false,
  //       code: 400,
  //       error: 'Group not found'
  //     }
  //   }

  //   const group = groupResult.group

  //   const studentListId = group.studentListId

  //   const studentListResult = await StudentList.getById(studentListId)
  //   const studentList = studentListResult.studentList.Item

  //   if (group.capacity <= studentList.list.L.length) {
  //     return {
  //       success: false,
  //       message: 'No se puede inscribir, cupo lleno',
  //       code: 400
  //     }
  //   }

  //   let groups
  //   let list = []

  //   if (student.groups) {
  //     if (student.groups.indexOf(id) === -1) {
  //       groups = student.groups.concat(id)
  //     } else {
  //       return {
  //         success: false,
  //         code: 400,
  //         error: 'Student already enrolled'
  //       }
  //     }
  //   } else {
  //     groups = [id]
  //   }

  //   await Students.update(studentId, { groups })
  //   if (studentList.list.L) {
  //     list = studentList.list.L
  //   }
  //   list = list.concat(studentId)

  //   await StudentList.update(studentListId, { list })

  //   if (data.payment) {
  //     data.payment['studentId'] = studentId
  //     data.payment['userId'] = user.userId
  //     data.payment['user'] = user
  //     data.payment['date'] = new Date().toISOString()

  //     if (data.payment['paymentType'] === 'card') {
  //       const paymentInfo = await PaymentInfo.create({
  //         cardNumber: data.payment['cardNumber'],
  //         expirationDate: data.payment['expirationDate'],
  //         cardType: data.payment['cardType'],
  //         cvv: data.payment['cvv']
  //       })

  //       if (!paymentInfo.success) {
  //         // return error
  //         return paymentInfo
  //       }

  //       paymentInfoId = paymentInfo.paymentInfoId
  //     }

  //     const payment = await Payments.create(data.payment)

  //     if (!payment.success) {
  //       // return error
  //       return payment
  //     }

  //     paymentId = payment.paymentId

  //     const credit = await Credits.create({
  //       amount: data.payment.amount,
  //       date: new Date().toISOString(),
  //       studentId
  //     })
  
  //     if (!credit.success) {
  //       // return error
  //       return credit
  //     }
  
  //     creditId = credit.creditId
  //   }

  //   return {
  //     success: true,
  //     enrolled: true,
  //     creditId,
  //     paymentInfoId,
  //     paymentId
  //   }
  // },

  // /**
  //  * getFromUserId
  //  *
  //  * @description: Gets the user's groups
  //  * @param {string} id - Group id.
  //  */
  // getFromUserId: async userId => {
  //   const user = await Users.getById(userId)

  //   if (!user.success) {
  //     return {
  //       code: 404,
  //       success: false,
  //       error: `User not found: ${userId}`
  //     }
  //   }

  //   const usersGroups = await UserGroups.getFromUser(userId)

  //   if (!usersGroups.success) {
  //     return {
  //       success: false,
  //       code: 404,
  //       error: `No groups for this user: ${userId}`
  //     }
  //   }

  //   const groupsIds = usersGroups.usersGroups.map(userGroup => userGroup.groupId)
  //   const groups = await Promise.all(groupsIds.map(id => Groups.getById(id)))
  //     .then(result => {
  //       return result.map(group => group.group.Item)
  //     })
  //     .catch(err => {
  //       console.error('- Error trying to get the groups from an user.', JSON.stringify(err, null, 2))
  //       return {
  //         success: false,
  //         code: 500,
  //         error: JSON.stringify(err, null, 2)
  //       }
  //     })

  //   return {
  //     success: true,
  //     groups
  //   }
  // },

  // getByCourseId: async courseId => {
  //   const params = {
  //     TableName: tableName,
  //     IndexName: 'CourseIndex',
  //     KeyConditionExpression: '#courseId = :courseId',
  //     ExpressionAttributeNames: {
  //       '#courseId': 'courseId'
  //     },
  //     ExpressionAttributeValues: {
  //       ':courseId': courseId
  //     }
  //   }

  //   const getGroups = docClient.query(params).promise()
  //   const groupsResult = await getGroups
  //     .then(groups => {
  //       return groups.Items
  //     })
  //     .catch(err => {
  //       console.error(`- Error trying to find groups by courseId (${courseId}).`, JSON.stringify(err, null, 2))
  //       return {
  //         success: false,
  //         code: 500,
  //         error: JSON.stringify(err, null, 2)
  //       }
  //     })

  //   const groups = await Promise.all(groupsResult.map(group => {
  //     return Groups.getAvailable(group)
  //   }))
  //     .then(result => result)

  //   return {
  //     success: true,
  //     groups
  //   }
  // }
}

module.exports = Groups
