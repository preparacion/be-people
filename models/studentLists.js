/**
 * models/studentLists.js
 *
 * @description :: Describes the studentLists functions
 * @docs        :: TODO
 */
const StudentListModel = require('../db/studentLists')

const StudentLists = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {
    const studentLists = await StudentListModel
      .find({})
      .then(studentLists => {
        return {
          success: true,
          studentLists
        }
      })
      .catch(err => {
        console.error('- Error trying to get all studentLists.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return studentLists
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - StudentList id.
   */
  getById: async id => {
    const studentList = await StudentListModel
      .findOne({ _id: id })
      .then(studentList => {
        return {
          success: true,
          studentList
        }
      })
      .catch(err => {
        console.error('- Error trying to get a studentList by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return studentList
  },

  /**
   * create
   *
   * @description: Create a new Item.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    const studentList = new StudentListModel(data)
    const result = await studentList.save()
      .then(studentList => {
        return {
          success: true,
          studentList
        }
      })
      .catch(err => {
        console.error('- Error trying to create a studentList', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * update
   *
   * @description: update an item.
   * @param {string} id - Item id
   * @param {object} data - List of attributes.
   */
  update: async (id, data) => {
    const studentList = await StudentLists.getById(id)
    
    if (!studentList.success) {
      return {
        success: false,
        code: 404,
        message: `StudentList not found ${id}`
      }
    }

    const result = await StudentListModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      })
      .then(studentList => {
        return {
          success: true,
          studentList
        }
      })
      .catch(err => {
        console.error('- Error trying to update a studentList', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Location id.
   */
  delete: async id => {
    const result = await StudentListModel.deleteOne({ _id: id })
      .then(() => {
        return {
          success: true,
          deleted: true
        }
      })
      .catch(err => {
        console.error('- Error trying to delete a studentList', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = StudentLists
