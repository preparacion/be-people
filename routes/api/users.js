/**
 * router/api/users.js
 *
 * @description :: Describes the users api routes
 * @docs        :: TODO
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

const Users = require('../../models/users')

router.prefix('/api/users')

// user validation schema
const userSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    lastName: Joi.string().min(1).max(100).required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().min(4).max(100),
    secondLastName: Joi.string().alphanum().min(1).max(100),
    birthDate: Joi.date(),
    phoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    registerType: Joi.string(),
    type: Joi.number().integer(),
    titularTarjeta: Joi.string().min(1).max(100),
    numeroDeCuenta: Joi.string().min(1).max(20),
    clabe: Joi.string().min(1).max(20),
    numeroTarjeta: Joi.string().min(1).max(16),
    banco: Joi.string().min(1).max(100),
    roleId: Joi.string()
      .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
  })

const updateUserSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100),
    lastName: Joi.string().min(1).max(100),
    email: Joi.string().email({ minDomainAtoms: 2 }),
    password: Joi.string().min(4).max(100),
    secondLastName: Joi.string().alphanum().min(1).max(100),
    birthDate: Joi.date(),
    phoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    registerType: Joi.string(),
    type: Joi.number().integer(),
    titularTarjeta: Joi.string().min(1).max(100),
    numeroDeCuenta: Joi.string().min(1).max(20),
    clabe: Joi.string().min(1).max(20),
    numeroTarjeta: Joi.string().min(1).max(16),
    banco: Joi.string().min(1).max(100),
    roleId: Joi.string()
      .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)
  })

const rolesUpdateSchema = Joi.object()
  .keys({
    role: Joi.string().default('')
  })

// Get all items
router.get('/', async (ctx, next) => {
  const result = await Users.get()

  if (result.success) {
    ctx.state.action = `Get all the users`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      users: result.users
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Users.getById(id)

  if (result.success) {
    ctx.state.action = `Get user by id ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      user: result.user
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, userSchema)

  if (validationResult.error === null) {
    // fixing dynamo date type
    if (validationResult.value.birthDate) {
      validationResult.value.birthDate = new Date(validationResult.value.birthDate)
        .toISOString()
    }

    const result = await Users.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `Create an user`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// update an user info
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateUserSchema)

  if (validationResult.error === null) {
    const result = await Users.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `Update an user ${id}`
      ctx.state.data = validationResult.value

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// update role in an user
router.put('/:id/role', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, rolesUpdateSchema)

  if (validationResult.error === null) {
    const result = await Users.updateRole(id, body.role)

    if (result.success) {
      ctx.state.action = `Update an user's role ${id}`
      ctx.state.data = body.role

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// delete a single item by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Users.delete(id)

  if (result.success) {
    ctx.state.action = `Delete an user ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get the user's groups
router.get('/:id/groups', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Users.getGroups(id)

  if (result.success) {

    ctx.state.action = `Get the user's groups ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.get('/phone/:phoneNumber', async (ctx, next) => {
  const phoneNumber = ctx.params.phoneNumber

  const result = await Users.findByPhone(phoneNumber)

  if (result.success) {
    ctx.state.action = `Find user by phone ${phoneNumber}`
    ctx.state.data = { phoneNumber }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
