/**
 * router/api/students.js
 *
 * @description :: Describes the students api routes
 * @docs        :: TODO
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

const Students = require('../../models/students')

const Util = require('../../util')

router.prefix('/api/students')

// Validation Schemas
const { groupIdSchema } = require('../../schemas/group')
const { paymentSchema } = require('../../schemas/payment')
const { studentSchema, updateStudentSchema } = require('../../schemas/student')
const { paginationSchema } = require('../../schemas/pagination')

const registerStudentSchema = Joi.object().keys({
  student: studentSchema.required(),
  group: groupIdSchema,
  payment: paymentSchema
})

// Get all items
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await Students.get(validationResult.value)

    if (result.success) {
      ctx.state.action = `Get all the students`
      ctx.state.data = {}

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Students.getById(id)

  if (result.success) {
    ctx.state.action = `Get student by id ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      student: result.student
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, studentSchema)

  if (validationResult.error === null) {
    // fixing dynamo date type
    if (validationResult.value.birthDate) {
      validationResult.value.birthDate = new Date(validationResult.value.birthDate)
        .toISOString()
    }
    const result = await Students.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `Create a student`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Create a student
router.post('/register', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, registerStudentSchema)

  if (validationResult.error === null) {
    const result = await Students.register(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a student`
      ctx.state.data = {}

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// update an student info
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateStudentSchema)

  if (validationResult.error === null) {
    // fixing dynamo date type
    if (validationResult.value.birthDate) {
      validationResult.value.birthDate = new Date(validationResult.value.birthDate)
        .toISOString()
    }
    
    const result = await Students.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `Update an student ${id}`
      ctx.state.data = validationResult.value

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// delete a single item by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Students.delete(id)

  if (result.success) {
    ctx.state.action = `Delete an student ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get the student's groups
router.get('/:id/groups', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Students.getGroups(id)

  if (result.success) {

    ctx.state.action = `Get the student's groups ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.get('/email/:email', async (ctx, next) => {
  const email = ctx.params.email

  const result = await Students.findByEmail(email)

  if (result.success) {
    ctx.state.action = `Find student by email ${email}`
    ctx.state.data = { email }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.get('/search/term', async (ctx, next) => {
  const q = ctx.query.q
  
  const result = await Students.find(q)

  if (result.success) {
    ctx.state.action = `Search student ${q}`
    ctx.state.data = { q }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
