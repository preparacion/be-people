/**
 * router/api/register.js
 *
 * @description :: Describes the register api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })
const Joi = require('joi')

const Register = require('../../models/register')

router.prefix('/api/register')

// user validation schema
const userSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    lastName: Joi.string().min(1).max(100).required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().min(4).max(100),
    phoneNumber: Joi.string()
  })

// Creates a new user
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, userSchema)

  if (validationResult.error === null) {
    const result = await Register.create(body)

    if (result.success) {
      ctx.state.action = 'user registration'
      ctx.state.data = body

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
