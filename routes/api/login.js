/**
 * router/api/login.js
 *
 * @description :: Describes the login api routes
 * @docs        :: TODO
 */
const jwt = require('jsonwebtoken')
const router = require('koa-router')({ sensitive: true })

const { jwtKey } = require('../../config')

const Login = require('../../models/login')

router.prefix('/api/login')

// Login
router.post('/', async (ctx, next) => {
  const { email, password } = ctx.request.body || {}

  const result = await Login.exec(email, password)

  if (result.success) {
    const user = result.user
    const token = jwt.sign({
      iat: Math.floor(Date.now() / 1000),
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 365),
      data: JSON.stringify(user)
    }, jwtKey)
    
    ctx.body = {
      success: true,
      message: 'login',
      user,
      token
    }
  } else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'Wrong user or password'
    }
  }

})

module.exports = router
