/**
 * router/api/me.js
 *
 * @description :: Describes the 'me' api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

router.prefix('/api/me')

router.get('/', async (ctx, next) => {
  if (ctx.state && ctx.state.user) {
    ctx.body = {
      success: true,
      user: ctx.state.user
    }
    return next()
  } else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})

module.exports = router
