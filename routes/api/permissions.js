/**
 * router/api/permissions.js
 *
 * @description :: Describes the permissions api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Permissions = require('../../models/permissions')
router.prefix('/api/permissions')

const updatePermissionSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  description: Joi.string().min(1).max(100),
  typePermission: Joi.number().integer()
})

// return all items
router.get('/', async (ctx, next) => {
  const result = await Permissions.get()
  if (result.success) {
    ctx.state.action = `Get all the permissions`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      permissions: result.permissions
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get an item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Permissions.getById(id)

  if (result.success) {
    ctx.state.action = `Get permission ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      role: result.permission
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create new Item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const result = await Permissions.create(body)

  if (result.success) {
    ctx.state.action = `Create a new permission`
    ctx.state.data = body

    ctx.status = 201
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Update an item
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updatePermissionSchema)

  if (validationResult.error === null) {
    const result = await Permissions.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `Update a permission ${id}`
      ctx.state.data = validationResult.value

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Delete an item
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Permissions.delete(id)

  if (result.success) {
    ctx.state.action = `Delete a permission ${id}`
    ctx.state.data = { id }
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
