/**
 * router/api/logout.js
 *
 * @description :: Describes the logout api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

router.prefix('/api/logout')

router.post('/', async (ctx, next) => {
  if (ctx.isAuthenticated()) {
    ctx.state.action = `logout`
    ctx.state.data = {}
    ctx.logout()

    ctx.body = {
      success: true,
      logout: 'logout'
    }
    return next()
  } else {
    ctx.status = 401
    ctx.body = {
      success: false
    }
  }
})

module.exports = router
