/**
 * router/api/home.js
 *
 * @description :: Describes the home api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

router.get('/', async (ctx, next) => {
  ctx.body = {
    success: true,
    message: 'be-courses, ok'
  }
  return next()
})

module.exports = router
