/**
 * router/api/roles.js
 *
 * @description :: Describes the roles api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Roles = require('../../models/roles')

router.prefix('/api/roles')

const createRoleSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    permissions: Joi.array().default([])
  })

const updatePermissionsSchema = Joi.object()
  .keys({
    permissions: Joi.array().default([])
  })

router.get('/', async (ctx, next) => {
  const result = await Roles.get()

  if (result.success) {
    ctx.body = {
      success: true,
      roles: result.roles
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Roles.getById(id)

  if (result.success) {
    ctx.state.action = `Get all the roles`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      role: result.role
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, createRoleSchema)

  if (validationResult.error === null) {
    const result = await Roles.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a new Role`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.put('/:id/permissions', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updatePermissionsSchema)

  if (validationResult.error === null) {
    const result = await Roles.updatePermissions(id, body.permissions)

    if (result.success) {
      ctx.state.action = `Update permissions in a role ${id}`
      ctx.state.data = body.permissions

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Roles.delete(id)

  if (result.success) {
    ctx.state.action = `Delete a role ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.get('/:id/users', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Roles.getUsers(id)

  if (result.success) {
    ctx.state.action = `Display users by role ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
