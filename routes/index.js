/**
 * routes/index.js
 *
 * @description :: Defines routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const isAuth = require('../middleware/isAuth')

// Home routes
const homeApi = require('./api/home')
router.use('', homeApi.routes(), homeApi.allowedMethods())
// Register routes
const registerApi = require('./api/register')
router.use('', registerApi.routes(), registerApi.allowedMethods())
// Login route
const loginApi = require('./api/login')
router.use('', loginApi.routes(), loginApi.allowedMethods())
// logout route
const logoutApi = require('./api/logout')
router.use('', logoutApi.routes(), logoutApi.allowedMethods())
// Permissions route
const permissionApi = require('./api/permissions')
router.use(isAuth, permissionApi.routes(), permissionApi.allowedMethods())
// Users route
const userApi = require('./api/users')
router.use('', userApi.routes(), userApi.allowedMethods())
// Roles routes
const roleApi = require('./api/roles')
router.use('', roleApi.routes(), roleApi.allowedMethods())
// Logs routes
const logsApi = require('./api/logs')
router.use('', logsApi.routes(), logsApi.allowedMethods())
// me
const meApi = require('./api/me')
router.use('', meApi.routes(), meApi.allowedMethods())
// Students routes
const studentsApi = require('./api/students')
router.use('', studentsApi.routes(), studentsApi.allowedMethods())

module.exports = router
